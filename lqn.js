function calculate() {
    var trungCap = document.getElementById("trungCap").value * 1;
    var caoDang = document.getElementById("caoDang").value * 1;
    var daiHoc = document.getElementById("daiHoc").value * 1;
    var trenDaiHoc = document.getElementById("trenDaiHoc").value * 1;

    var total = trungCap + caoDang + daiHoc + trenDaiHoc;
    var per1 = (trungCap / total * 100).toFixed(1);
    var per2 = (caoDang / total * 100).toFixed(1);
    var per3 = (daiHoc / total * 100).toFixed(1);
    var per4 = (trenDaiHoc / total * 100).toFixed(1);

    var mainPer1 = parseInt(per1) + parseInt(per2);
    var mainPer2 = mainPer1 + parseInt(per3);

    document.getElementById("total").innerHTML = total;

    document.getElementById("perTrungCap1").innerHTML = per1;
    document.getElementById("perTrungCap2").innerHTML = per1;
    
    document.getElementById("perCaoDang1").innerHTML = per2;
    document.getElementById("perCaoDang2").innerHTML = per2;
    
    document.getElementById("perDaiHoc1").innerHTML = per3;
    document.getElementById("perDaiHoc2").innerHTML = per3;
    
    document.getElementById("perTrDaiHoc1").innerHTML = per4;
    document.getElementById("perTrDaiHoc2").innerHTML = per4;

    document.getElementById("mainPer").innerHTML = per1;
    document.getElementById("mainPer1").innerHTML = mainPer1;
    document.getElementById("mainPer2").innerHTML = mainPer2;
    document.getElementById("mainPer3").innerHTML = 100;
    

}